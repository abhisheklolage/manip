from enum import Enum

def get_voting_scheme_name(i):
    v = VotingSchemeOption()
    if (i == v.PLURALITY_VOTING):
        return 'PLURALITY'
    if (i == v.ANTI_PLURALITY_VOTING):
        return 'ANTI_PLURALITY'
    if (i == v.VOTING_FOR_TWO):
        return 'VOTING_FOR_TWO'
    if (i == v.BORDA):
        return 'BORDA'
    if (i == v.FIRST_ORDER_COPELAND):
        return 'FIRST_ORDER_COPELAND'
    if (i == v.SECOND_ORDER_COPELAND):
        return 'SECOND_ORDER_COPELAND'

class VotingSchemeOption(int):
    PLURALITY_VOTING = 0
    ANTI_PLURALITY_VOTING = 1
    VOTING_FOR_TWO = 2
    BORDA = 3
    FIRST_ORDER_COPELAND = 4
    SECOND_ORDER_COPELAND = 5

class VotingScheme:

    def __init__(self, preferences, n_candidates):
        self.preferences = preferences
        self.voting = {}
        self.nCandidates = n_candidates
        self.reset_voting()
        self.overall_happiness = 0
        self.candidates = []
        for candidate in range(ord('A'), ord('A') + self.nCandidates):
            self.candidates.append(chr(candidate))

    def reset_voting(self):
        # Dictionary with the output of the vote
        for candidate in range(ord('A'), ord('A') + self.nCandidates):
            self.voting[chr(candidate)] = 0



    """
    Only the first candidate in the preference list gets a vote
    """
    def plurality_voting(self):
        self.reset_voting()
        for voter, voter_preference in self.preferences.items():
            self.voting[voter_preference[0]] += 1
        return self.voting

    """
    Only two first candidates in the preference list get a vote
    """
    def voting_for_two(self):
        self.reset_voting()
        for voter, voter_preference in self.preferences.items():
            self.voting[voter_preference[0]] += 1
            if voter_preference[1] is not "":
                self.voting[voter_preference[1]] += 1
        return self.voting

    """
    All the candidates in the preference list get a vote except the last one
    """
    def anti_plurality_voting(self):
        self.reset_voting()
        for voter, voter_preference in self.preferences.items():
            for i, p in enumerate(voter_preference):
                if i is len(voter_preference) - 1 or p is "":
                    break
                self.voting[p] += 1
        return self.voting

    """
    If there are N candidates, the first in the preference list gets N points, the second N - 1, etc
    """
    def borda_voting(self):
        self.reset_voting()
        for voter, voter_preference in self.preferences.items():
            for i, p in enumerate(voter_preference):
                if p is "":
                    break
                self.voting[p] += len(voter_preference) - 1 - i
        return self.voting

    def pref_compare_two(self, pref, c1, c2):
        # check which appears in preference first
        for i, p in enumerate(pref):
            if p is c1:
                return c1
            elif p is c2:
                return c2

    def fo_copland(self):
        defeats = {}
        fo_copland_scores = {}
        for candidate in range(ord('A'), ord('A') + self.nCandidates):
            defeats[chr(candidate)] = []
            fo_copland_scores[chr(candidate)] = 0

        for p1 in range(len(self.candidates)):
            for p2 in range(p1 + 1, len(self.candidates)):
                c1_count, c2_count = 0, 0
                c1 = self.candidates[p1]
                c2 = self.candidates[p2]
                for _, pref in self.preferences.items():
                    pairwise_winner = self.pref_compare_two(pref, c1, c2)
                    if pairwise_winner is c1:
                        c1_count += 1
                    else:
                        c2_count += 1
                if c1_count > c2_count:
                    fo_copland_scores[c1] += 1
                    fo_copland_scores[c2] -= 1
                    defeats[c1].append(c2)
                elif c2_count > c1_count:
                    fo_copland_scores[c2] += 1
                    fo_copland_scores[c1] -= 1
                    defeats[c2].append(c1)
                else:
                    fo_copland_scores[c1] += 0.5
                    fo_copland_scores[c2] += 0.5
                    defeats[c1].append(c2)
                    defeats[c2].append(c1)

        return fo_copland_scores, defeats

    def get_ties(self, scores):
        outcome = self.get_outcome_for_list(scores)
        tie_between = []
        winner_score = scores[outcome[0]]
        for c in outcome:
            if scores[c] == winner_score:
                tie_between.append(c)
        return tie_between

    def first_order_copeland_voting(self):
        self.reset_voting()
        fo_copland_scores, defeats = self.fo_copland()
        self.voting = fo_copland_scores
        return self.voting

    def second_order_copeland_voting(self):
        self.reset_voting()

        fo_copland_scores, defeats = self.fo_copland()
        tie_between = self.get_ties(fo_copland_scores)
        if len(tie_between) == 1:
            self.voting = fo_copland_scores
            return self.voting

        so_copland_scores = {}
        for c in self.candidates:
            so_copland_scores[c] = -len(self.candidates)

        for c in tie_between:
            so_copland_scores[c] = 0
            for defeated in defeats[c]:
                so_copland_scores[c] += fo_copland_scores[defeated]

        self.voting = so_copland_scores
        return self.voting

    def get_outcome(self):
        # return candidates based on descending score, lexicographic when tie
        return sorted(sorted(self.voting), key=self.voting.get, reverse=True)

    def get_outcome_for_list(self, l):
        # return candidates based on descending score, lexicographic when tie
        return sorted(sorted(l), key=l.get, reverse=True)

    def calc_happiness(self, outcome):
        '''
        Possible notion of profitable manipulation
        '''
        happiness = {}
        winner = outcome[0]
        old_outcome = self.get_outcome()
        new_outcome = outcome

        for voter, voter_preferences in self.preferences.items():
            happiness[voter] = self.nCandidates - (voter_preferences.index(winner) + 1)
            # print('voter', voter, 'happiness is ', happiness[voter])
        return happiness

    def calc_overall_happiness(self, outcome):
        overall = 0
        for happiness in self.calc_happiness(outcome).values():
            overall += happiness
        return overall

    def get_happiness_by_voter(self, voter):
        return self.calc_happiness(self.get_outcome())[voter]

    def get_new_happiness_by_voter(self, voter, new_outcome):
        return self.calc_happiness(new_outcome)[voter]

    def is_happy(self, happiness):
        if happiness == self.nCandidates - 1:
            return True
        else:
            return False

    def execute_voting(self, voting_scheme):
        if voting_scheme is VotingSchemeOption.PLURALITY_VOTING:
            return self.plurality_voting()
        elif voting_scheme is VotingSchemeOption.ANTI_PLURALITY_VOTING:
            return self.anti_plurality_voting()
        elif voting_scheme is VotingSchemeOption.VOTING_FOR_TWO:
            return self.voting_for_two()
        elif voting_scheme is VotingSchemeOption.BORDA:
            return self.borda_voting()
        elif voting_scheme is VotingSchemeOption.FIRST_ORDER_COPELAND:
            return self.first_order_copeland_voting()
        elif voting_scheme is VotingSchemeOption.SECOND_ORDER_COPELAND:
            return self.second_order_copeland_voting()
