import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from random import sample
from manipulator import Manipulator
import numpy as np
import itertools
import math
from voting_scheme import get_voting_scheme_name

# The + 1 is because when using range the last value is not used
# Therefore, range (2,4) would give only 2 and 3 when we also want the 4
maximum_n_voters = 5 + 1
minimum_n_voters = 2

maximum_n_candidates = 3 + 1
minimum_n_candidates = 2

number_of_times_execute = 1

history = np.zeros((maximum_n_voters, maximum_n_candidates))
risk_history = [0, 0, 0, 0, 0, 0]
manipulable_elections = [0, 0, 0, 0, 0, 0]
strategic_pts = [0, 0, 0, 0, 0, 0]

n_candidates = 4
n_voters = 6
candidates = [chr(i) for i in range(ord('A'), ord('A') + n_candidates)]

all_prefs = [list(p) for p in itertools.permutations(candidates)]
#all_elections = [list(e) for e in itertools.product(all_prefs, repeat=n_voters)]

num_schemes = 6

def get_risk_plot(n_times):
    election_n = 0
    for election in itertools.product(all_prefs, repeat=n_voters):
        #print("election #", election_n + 1, "/", n_times)
        #preferences = get_random_preferences(n_voters, candidates)
        preferences = get_preferences(list(election), n_voters)
        for voting_scheme_option in range(num_schemes):
            model = Manipulator(preferences, voting_scheme_option)
            outcome, overall_happiness, strategic_voting_option, str_pts = model.calculate(False)
            # print(all_elections[i], strategic_voting_option)
            risk_history[voting_scheme_option] += (str_pts / (math.factorial(n_candidates) * n_voters))
            if str_pts > 0:
                manipulable_elections[voting_scheme_option] += 1
                strategic_pts[voting_scheme_option] += str_pts
        election_n += 1

    print(manipulable_elections, "out of", n_times, "elections are manipulable")
    print(strategic_pts, "strategic pts over", n_times, "elections")
    # Calculate the average
    for voting_scheme_option in range(num_schemes):
        risk_history[voting_scheme_option] = risk_history[voting_scheme_option] / n_times
    print(risk_history)
    hist = ["Plurality", "Anti-plurality", "Voting for two", "Borda voting"]
    plt.bar([0, 1, 2, 3, 4, 5], risk_history)


def get_random_preferences(n_voters, candidates):
    preferences = {}
    for voter in range(int(n_voters)):
        preferences[voter] = sample(candidates, len(candidates))
    return preferences

def get_ith_preferences(i):
    preferences = {}
    for voter in range(int(n_voters)):
        preferences[voter] = all_elections[i][voter]
    return preferences

def get_preferences(prefs, __n_voters):
    preferences = {}
    for voter in range(int(__n_voters)):
        preferences[voter] = prefs[voter]
    return preferences

def calculate_risks(voting_scheme_option):
    for __n_voters in range(minimum_n_voters, maximum_n_voters):
        for __n_candidates in range(minimum_n_candidates, maximum_n_candidates):
            __candidates = [chr(i) for i in range(ord('A'), ord('A') + __n_candidates)]
            __all_pref = [list(p) for p in itertools.permutations(__candidates)]
            election_n = 0
            __n_times = math.factorial(__n_candidates)**__n_voters
            risk_per_election = 0.0
            total_str_pts = 0
            manipulable_elections = 0
            for __election in itertools.product(__all_pref, repeat=__n_voters):
                #print("election #", election_n + 1, "/", __n_times)
                #preferences = get_random_preferences(n_voters, candidates)
                preferences = get_preferences(list(__election), __n_voters)
                #preferences = get_random_preferences(n_voters, candidates)
                #preferences = get_ith_preferences()
                # print(preferences)
                model = Manipulator(preferences, voting_scheme_option)
                outcome, overall_happiness, strategic_voting_option, str_pts = model.calculate(False)
                total_str_pts += str_pts
                if (str_pts > 0):
                    manipulable_elections += 1
                risk_per_election += (str_pts / (math.factorial(__n_candidates) * __n_voters))

                # print("Number of voters is", n_voters, "and number of candidates is", __n_candidates, "the risk is", risk)
                # max(risk, history[n_voters, __n_candidates])
                # print(history)
                election_n += 1
            #history[__n_voters, __n_candidates] = risk_per_election  / __n_times
            history[__n_voters, __n_candidates] = total_str_pts
            print("nvoters", __n_voters, "ncand", __n_candidates, total_str_pts, "/", __n_times * __n_voters * (math.factorial(__n_candidates) - 1), "str pts",
                  manipulable_elections, "/", __n_times, "manipulable elections")

def calculate_average_risk(voting_scheme_option):
    print()
    print("Apply voting scheme:", voting_scheme_option)
    calculate_risks(voting_scheme_option)


def create_plot(voting_scheme_option):

    ###
    fname = "./figs/" + get_voting_scheme_name(voting_scheme_option) + str("_with_nvoters_fixed") + str(".png")
    for n_voters in range(minimum_n_voters, maximum_n_voters):
        risk_to_represented = history[n_voters]
        plt.plot(range(len(risk_to_represented)), risk_to_represented, label=(str(n_voters) + " Voters"))

    plt.xlabel("Number of candidates")
    plt.ylabel("Maximum number of strategic options")
    if voting_scheme_option == 0:
        plt.title("Applied plurality voting")
    elif voting_scheme_option == 1:
        plt.title("Applied anti-plurality voting")
    elif voting_scheme_option == 2:
        plt.title("Applied voting for 2")
    elif voting_scheme_option == 3:
        plt.title("Applied Borda")
    elif voting_scheme_option == 4:
        plt.title("Applied First Order Copeland")
    elif voting_scheme_option == 5:
        plt.title("Applied Second Order Copeland")
    plt.legend()
    plt.savefig(fname)
    plt.clf()

    ###
    fname = "./figs/" + get_voting_scheme_name(voting_scheme_option) + str("_with_ncandidates_fixed") + str(".png")
    for n_candidates in range(minimum_n_candidates, maximum_n_candidates):
        risk_to_represented = history[:, n_candidates]
        plt.plot(range(len(risk_to_represented)), risk_to_represented, label=(str(n_candidates) + " Candidates"))

    plt.xlabel("Number of voters")
    plt.ylabel("Maximum number of strategic options")
    plt.title("Effect of increasing the number of voters in the risk")
    if voting_scheme_option == 0:
        plt.title("Applied plurality voting")
    elif voting_scheme_option == 1:
        plt.title("Applied anti-plurality voting")
    elif voting_scheme_option == 2:
        plt.title("Applied voting for 2")
    else:
        plt.title("Applied Borda")

    plt.legend()
    plt.savefig(fname)
    plt.clf()


for option in range(6):
    calculate_average_risk(option)
    create_plot(option)
    history = np.zeros((maximum_n_voters, maximum_n_candidates))

# print("Shape of history", history.shape)
# print("Number of voters", maximum_n_voters)
# print("Number of candidates", maximum_n_candidates)

#plt.legend()
get_risk_plot((math.factorial(n_candidates))**n_voters)
# plt.show()
