import unittest
from voting_scheme import VotingScheme

class VotingSchemeShould(unittest.TestCase):

    def setUp(self):
        self.nCandidates = 5
        self.preferences = {0: ['A', 'D', 'E', 'B', 'C'], 1: ['C', 'B', 'E', 'A', 'D'], 2: ['E', 'D', 'A', 'B', 'C'],
                       3: ['C', 'E', 'A', 'D', 'B'], 4: ['E', 'C', 'D', 'A', 'B']}
        self.voting = VotingScheme(self.preferences, self.nCandidates)

    def test_plurality_voting(self):
        print("Testing Plurality")
        expected_outcome = {'A': 1, 'B': 0, 'C': 2, 'D': 0, 'E': 2}
        actual_outcome = self.voting.plurality_voting()
        self.assertEqual(expected_outcome, actual_outcome)

    def test_voting_for_two(self):
        print("Testing Voting For Two")
        expected_outcome = {'A': 1, 'B': 1, 'C': 3, 'D': 2, 'E': 3}
        actual_outcome = self.voting.voting_for_two()
        self.assertEqual(expected_outcome, actual_outcome)

    def test_anti_plurality_voting(self):
        print("Testing Anti Plurality")
        expected_outcome = {'A': 5, 'B': 3, 'C': 3, 'D': 4, 'E': 5}
        actual_outcome = self.voting.anti_plurality_voting()
        self.assertEqual(expected_outcome, actual_outcome)

    def test_borda_voting(self):
        print("Testing Borda")
        expected_outcome = {'A': 10, 'B': 5, 'C': 11, 'D': 9, 'E': 15}
        actual_outcome = self.voting.borda_voting()
        self.assertEqual(expected_outcome, actual_outcome)

    def test_get_outcome(self):
        expected_outcome = ['C', 'E', 'A', 'B', 'D']

        self.voting.plurality_voting()
        actual_outcome = self.voting.get_outcome()

        self.assertEqual(expected_outcome, actual_outcome)

class FirstOrderCopelandTest(unittest.TestCase):
    def setUp(self):
        self.nCandidates = 3
        self.preferences = { 0: ['A', 'C', 'B'],
                             1: ['A', 'C', 'B'],
                             2: ['A', 'C', 'B'],
                             3: ['C', 'B', 'A'],
                             4: ['C', 'B', 'A'],
                             5: ['C', 'A', 'B'],
                             6: ['B', 'A', 'C'] }
        self.voting = VotingScheme(self.preferences, self.nCandidates)

    def test_first_order_copeland(self):
        print("Testing First Order Copeland")
        expected_outcome = {'A': 2, 'B': -2, 'C': 0}
        actual_outcome = self.voting.first_order_copeland_voting()
        self.assertEqual(expected_outcome, actual_outcome)

class SecondOrderCopelandTest(unittest.TestCase):
    def setUp(self):
        self.nCandidates = 4
        self.preferences = { 0: ['A', 'B', 'C', 'D'],
                             1: ['A', 'B', 'C', 'D'],
                             2: ['A', 'B', 'C', 'D'],
                             3: ['D', 'B', 'C', 'A'],
                             4: ['D', 'B', 'C', 'A'],
                             5: ['D', 'B', 'A', 'C'],
                             6: ['C', 'D', 'A', 'B'] }
        self.voting = VotingScheme(self.preferences, self.nCandidates)

    def test_second_order_copeland(self):
        print("Testing Second Order Copeland")
        expected_outcome = {'D': 0, 'A': -2, 'B': -4, 'C': -4}
        actual_outcome = self.voting.second_order_copeland_voting()
        self.assertEqual(expected_outcome, actual_outcome)

